@extends('layout.master')
@section('judul')

<h1>Buat Account Baru</h1>

@endsection
@section('content')

    <h4>Sign Up Form</h4>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="name">
        <br>
        <label>Last Name :</label><br>
        <input type="text" name="lastname">
        <br><br>
        <label>Gender : </label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br>
        <br>
        <label>Nationality : </label>
            <select name="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Singapura">Singapura</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Thailand">Thailand</option>
            </select>
        <br><br>
        <label>Language Spoken : </label><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Arabic<br>
        <input type="checkbox" name="bahasa">Japanese<br><br>
        <Label>Bio</label><br>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br>
        <input type="submit" value="kirim">
    </form>
@endsection